--- 
title: "Bookdown Mathplosion"
author: "Mathplosion"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
geometry: margin=3cm
fontsize: 12pt
documentclass: book
bibliography: [websites.bib, book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
github-repo: 
description: "This is the bookdown template for mathplosion examples"
---

```{r setup, echo=FALSE}
source("bookdown-mathplosion.R")
knit_examples_init("1")
```

# Prereqs {#prereqs} 

- source here: [index.Rmd](index.Rmd)
- md source: [index.md](index.md)
  

This bookdown template shows how mathplosion works with bookdown. There are chapters for the book, each chapters is a different Rmd file. You can see the the Rmd for this chapter at the link above.  

There are 4 chapters and a reference (or bibliography). 

Some of the chapters (2 and 3) have examples, which are worked problems. Each example is an instance of a mathplosion problem with some parameters specified. These mathplosion problems are also Rmd files, and they are stored in a directory called `problems` in the main project directory. Each time an example is created, the name of the mathplosion problem to use is specified, and the Rmd file for the problem is called with the specified parameters. Then the rendered markdown and graphics for that problem are inserted into the book as that example.  

To see examples of the mathplosion problems: 

- [Chapter 2](#examples) has calculations
- [Chapter 3](#graphics) has examples with graphics

Here is what the code looks like to call a mathplosion problem:  

    `​``{r example=TRUE, title="Find the z-value", results='asis'}
    knit_example('problems/ZValue.Rmd', list(mu=52, x=54, sigma=8))
    ```
    
We make code chunk for the problem and set the `problem=TRUE` option. This means the Rmd for the mathplosion problem will be run and inserted into the narrative. The mathplosion problem being used here is called `ZValue.Rmd`. This file is in the `problems` directory. The parameters being passed to this problem are `mu`, `x`, and `sigma`. The function that reads the file, executes it and inserts the content is called `knit_example`. The title of the example is "Find the z-value". Here is the result of the above:  

```{r example=TRUE, title="Find the z-value", results='asis'}
knit_example('problems/ZValue.Rmd', list(mu=52, x=54, sigma=8))
```

The source for this mathplosion problem is here: [ZValue.Rmd](problems/ZValue.Rmd) 

What if we want an example that is just markdown based and should not be a mathplosion problem? We can do this...

Use the "asis" engine to just pass it through.

    `​``{asis example=TRUE, title="Population vs Sample", echo=TRUE}
    Is a parameter or a statistic if we collect data from just the class and not all of FIT?   
    It is a __statistic__. And the symbol for it would be:
    \begin{equation}
       \hat{p}=\frac{12}{20} (\#eq:blah)
    \end{equation}
    This is the sample proportion. 
    ```

Here is what it looks like...

```{asis example=TRUE, title="Population vs Sample", echo=TRUE}
Is a parameter or a statistic if we collect data from just the class and not all of FIT?   
It is a __statistic__. And the symbol for it would be:

\begin{equation}
    \hat{p}=\frac{12}{20} (\#eq:blah)
\end{equation}

This is the sample proportion. 
```

This allows us to mix programmatic mathplosion examples with regular just one-off examples.
