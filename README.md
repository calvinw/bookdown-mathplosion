This is a mathplosion related bookdown project. It started off as the minimal example of bookdown

To build it you need to run the following in a shell:

```
make
make pdf
```

The built version is hosted on the pages url:

https://calvinw.gitlab.io/bookdown-mathplosion/

For using other latex engines (not Mathjax) you need to set the `pandoc_args` to indicate which webtex option you will use, so in _output.yml:

```
bookdown::gitbook:
    //this will give you google latex 
  pandoc_args: ["--webtex=http://chart.googleapis.com/chart?cht=tx&chl="]
  mathjax: null

  or ...

  //this will give you codecogs png latex 
  pandoc_args: ["--webtex=https://latex.codecogs.com/png.latex?"]
  mathjax: null

  or ...

  pandoc_args: [""]
```

Note that this will mean that refs and labels and equation, figure and table numbering will not work, since those LaTeX engines do not support that kind of thing. 
