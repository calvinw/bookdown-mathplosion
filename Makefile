SHELL:=/bin/bash
SOURCES=$(shell find . -maxdepth 1 -name "*.Rmd" -not -name "index.Rmd")

HTML_FILES = $(SOURCES:%.Rmd=_book/%.html)
MD_FILES = $(SOURCES:%.Rmd=_book/%.md)

all: _book/index.html $(HTML_FILES)

clean :
	rm -rf _book/* _bookdown_files bookdown_mathplosion.rds

_book/%.html : %.Rmd
	Rscript -e "bookdown::preview_chapter('$<')"
	cp $< _book
	cp -r problems/ _book

_book/index.html : index.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"
	cp *.Rmd _book
	cp -r problems/ _book

pdf : index.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_book')"

epub : index.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::epub_book')"

.PHONY: all clean
